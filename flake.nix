{
  description = "Aeltheos personal infrastructure";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    terranix.url = "github:terranix/terranix";
    terranix-libvirt.url = "github:ybeaugnon/terranix-libvirt";
    nixos-generators.url = "github:nix-community/nixos-generators";
  };

  outputs = { self, nixpkgs, terranix, flake-utils, terranix-libvirt, nixos-generators }: flake-utils.lib.eachDefaultSystem
    (system:
      let
        pkgs = import nixpkgs { inherit system; };

        terraform-config = terranix.lib.terranixConfiguration {
          inherit system;
          modules = [
            ./infra
            terranix-libvirt.nixosModules.terranix-libvirt
            { _module.args = { inherit nixos-generators; }; }
          ];
        };
      in
      {
        formatter = pkgs.nixpkgs-fmt;

        devShells.default = pkgs.mkShell {
          buildInputs = [
            (pkgs.terraform.withPlugins (p: [ p.libvirt ]))
          ];
        };

        packages.default = terraform-config;
      }) // { };
}
