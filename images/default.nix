{ nixpkgs, nixos-generators, ... }:
let
  base = import ../modules/base.nix;
in
{
  router = nixos-generators.nixosGenerate {
    system = "x86_64-linux";
    modules = [
      base
      { networking.hostName = "router"; }
    ];
    format = "qcow";
  };

  service-1 = nixos-generators.nixosGenerate {
    system = "x86_64-linux";
    modules = [
      base
      { networking.hostName = "service-1"; }
    ];
    format = "qcow";
  };
}

