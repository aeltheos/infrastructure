This is my personal service infracstructure, using nixos and terraform to deploy libvirt vms.

> To build and deploy
```bash
nix build git+ssh://git@gitlab.crans.org/aeltheos/infrastructure -o config.tf.json
terraform plan -out out
terraform apply out
```