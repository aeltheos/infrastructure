{ config, lib, nixos-generators, nixpkgs, ... }: with lib;
let
  cfg = config.libvirt;

  images = import ../images { inherit nixpkgs nixos-generators; };
in
{
  # Murozond configuration
  config.libvirt.providers.murozond.uri =
    "qemu+ssh://root@murozond.adh.crans.org/system?sshauth=agent&no_verify=1";

  config.libvirt.networks.murozond-adm = {
    mode = "none";
    addresses = [ "10.0.0.0/24" ];
    provider = "libvirt.murozond";
  };

  # images
  config.libvirt.volumes.router-img-murozond = {
    source = "${images.router}/nixos.qcow2";
    provider = "libvirt.murozond";
  };

  config.libvirt.volumes.service-1 = {
    source = "${images.service-1}/nixos.qcow2";
    provider = "libvirt.murozond";
  };

  # domains
  config.libvirt.domains.router-murozond = {
    disks = [
      { volume_id = cfg.volumes.router-img-murozond.id; }
    ];

    networkInterfaces = [
      { network_id = cfg.networks.murozond-adm.id; }
      {
        macvtap = "eno1";
        mac = "b6:2f:c7:52:b8:61";
      }
    ];
    provider = "libvirt.murozond";
  };

  config.libvirt.domains.server-1 = {
    disks = [
      { volume_id = cfg.volumes.service-1.id; }
    ];

    networkInterfaces = [
      { network_id = cfg.networks.murozond-adm.id; }
    ];

    provider = "libvirt.murozond";
  };
}
