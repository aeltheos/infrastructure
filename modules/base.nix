{ config, lib, ... }: with lib;
let
  cfg = config;
in
{
  config.system.stateVersion = "23.11";
  config.users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHc3PC5Cl1AI5jV1glN8TyKZtdaIPKZeF/45ryviRPO8 aeltheos@malygos"
  ];

  config.services.openssh.enable = true;

  config.networking.firewall.allowedTCPPorts = [ 22 ];
  config.networking.firewall.allowedUDPPorts = [ 22 ];
}
